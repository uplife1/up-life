import 'package:flutter/material.dart';
import 'package:up_life/ui/pages/cafedamanha.page.dart';
import 'almoco.page.dart';
import 'janta.page.dart';

class DietaPage extends StatefulWidget {
  const DietaPage({Key? key}) : super(key: key);

  @override
  _DietaPageState createState() => _DietaPageState();
}

class _DietaPageState extends State<DietaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alimentação Saudável'),
      ),
      body: Container(
        color: Colors.grey[300],
        child: ListView(
          children: <Widget>[
            Container(
              color: Colors.grey[300],
              height: MediaQuery.of(context).size.height * 0.4,
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).size.height * 0.01,
                  top: MediaQuery.of(context).size.height * 0.1),
              child: Image.asset('images/dieta.png'),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.2,
                  left: MediaQuery.of(context).size.width * 0.05,
                  right: MediaQuery.of(context).size.width * 0.05),
              child: ElevatedButton(
                child: Text(
                  "Café da Manhã",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CafeDaManhaPage()));
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.05,
                  right: MediaQuery.of(context).size.width * 0.05),
              child: ElevatedButton(
                child: Text(
                  "Almoço",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AlmocoPage()));
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.05,
                  right: MediaQuery.of(context).size.width * 0.05),
              child: ElevatedButton(
                child: Text(
                  "Janta",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => JantaPage()));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
