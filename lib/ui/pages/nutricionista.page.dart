import 'package:flutter/material.dart';
import 'package:up_life/ui/pages/chat.page.dart';

class NutriPage extends StatefulWidget {
  const NutriPage({Key? key}) : super(key: key);

  @override
  _NutriPageState createState() => _NutriPageState();
}

class _NutriPageState extends State<NutriPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Fale com um nutricionista"),
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(60),
            ),
            Container(
              height: 240,
              child: Image.asset('images/man.png'),
            ),
            Text(
              '\nO nutricionista Jorge está online',
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headline6!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.all(30),
              child: ElevatedButton(
                child: Text(
                  "Fale com o Nutricionista",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ChatPage()));
                },
              ),
            ),
          ],
        ));
  }
}
