import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class LeitorController extends GetxController {
  var valorCodigoBarras = '';
  var nome_produto = '';
  var calorias = '';
  var avaliacao = "";

  Future<void> escanear_cod() async {
    String scanner_cod = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666', 'Cancelar', true, ScanMode.BARCODE);

    if (scanner_cod == '-1') {
      Get.snackbar('Cancelado', 'Leiturada Cancelada');
    } else if (scanner_cod == '7896445490550') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Água com gás';
      avaliacao = 'Produto Saudável';
      update();
    } else if (scanner_cod == '070847033301') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Monster de Manga';
      calorias = '104 calorias por porção(200ml)';
      avaliacao = 'Produto não Saudável';
      update();
    } else if (scanner_cod == '7896009301049') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Sardinhas Com Óleo';
      calorias = '108 calorias por porção(60g)';
      avaliacao = 'Produto Saudável';
      update();
    } else if (scanner_cod == '7896028085654') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Coco em flocos adoçado ';
      calorias = ' 68 calorias por porção(12g)';
      avaliacao = 'Produto não Saudável';
      update();
    } else if (scanner_cod == '7891000065440') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Leite Condensado';
      calorias = '63 calorias por porção(20g)';
      avaliacao = 'Produto não Saudável';
      update();
    } else if (scanner_cod == '7896292358072') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Mix de milho e ervilhas';
      calorias = '118 calorias por porção(130g)';
      avaliacao = 'Produto Saudável';
      update();
    } else if (scanner_cod == '7894321711263') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Achocolatado Toddy';
      calorias = '77 calorias por porção(20g)';
      avaliacao = 'Produto não Saudável';
      update();
    } else if (scanner_cod == '7896066304571') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Pão integral ';
      calorias = '131 calorias por porção(50g)';
      avaliacao = 'Produto Saudável';
      update();
    } else if (scanner_cod == '7892840816834') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Salgadinho Cheetos Bola';
      calorias = '204 calorias por porção(42g)';
      avaliacao = 'Produto não Saudável';
      update();
    } else if (scanner_cod == '7891991002561') {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Guaraná Antártica';
      calorias = '83 calorias por porção(200ml)';
      avaliacao = 'Produto não Saudável';
      update();
    } else {
      valorCodigoBarras = scanner_cod;
      nome_produto = 'Produto Não Encontrado';
      calorias = '';
      avaliacao = '';
      update();
    }
  }
}
