import 'package:flutter/material.dart';
import 'package:up_life/ui/pages/carrega_page.dart';
import 'package:up_life/ui/pages/login.page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

/*class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}*/

class _HomePageState extends State<HomePage> {
  late bool _carregando;

  @override
  void initState() {
    _carregando = true;
  }

  @override
  Widget build(BuildContext context) {
    // ignore: dead_code
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        _carregando = false;
      });
    });
    return _carregando ? CarregaPage() : Login();
  }
}
