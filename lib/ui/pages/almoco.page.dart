import 'package:flutter/material.dart';

class AlmocoPage extends StatefulWidget {
  const AlmocoPage({Key? key}) : super(key: key);

  @override
  _AlmocoPageState createState() => _AlmocoPageState();
}

class _AlmocoPageState extends State<AlmocoPage> {
  l_card() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank_outlined, size: 60),
            title: Text('Frango com legumes'),
            subtitle: Text('150g de filé de frango, legumes a vontade'),
          ),
        ],
      ),
    );
  }

  l_card2() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank_outlined, size: 60),
            title: Text('Lanche Natural'),
            subtitle: Text('100g de frango desfiado, 20g de cenoura'),
          ),
        ],
      ),
    );
  }

  l_card3() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank_outlined, size: 60),
            title: Text('Omelete'),
            subtitle: Text('3 ovos, 30g de aveia, 50g de frango'),
          ),
        ],
      ),
    );
  }

  l_card4() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank_outlined, size: 60),
            title: Text('Carne Moída'),
            subtitle: Text('100g de carne moída, 50g de arroz, salada'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Almoço"),
        ),
        body: Column(children: <Widget>[
          Text('\nDicas de Almoço\n',
              style: Theme.of(context).textTheme.headline5!),
          l_card(),
          l_card2(),
          l_card3(),
          l_card4(),
        ]));
  }
}
