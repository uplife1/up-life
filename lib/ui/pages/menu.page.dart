import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:up_life/ui/pages/calculoagua.page.dart';
import 'package:up_life/ui/pages/calculoimc.page.dart';
import 'package:up_life/ui/pages/dieta.page.dart';
import 'package:up_life/ui/pages/home_screen.dart';
import 'package:up_life/ui/pages/leitor.page.dart';
import 'package:up_life/ui/pages/login.page.dart';
import 'package:up_life/ui/pages/maps.page.dart';
import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:up_life/ui/pages/nutricionista.page.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  List yourList = [
    "Evite o consumo de ultraprocessados.",
    "Desenvolva, exercite e partilhe habilidades culinárias.",
    "Utilize óleos, gorduras, sal e açúcar em pequenas quantidades.",
    "Pratique exercicios físicos diariamente."
        "Mantenha-se hidratado."
        "Evite o consumo de bebidas alcólicas"
  ];
  var result = "";

  @override
  Widget build(BuildContext context) {
    setState(() {
      int randomIndex = Random().nextInt(yourList.length);
      print(yourList[randomIndex]);
      result = yourList[randomIndex];
    });

    return Stack(
      children: <Widget>[
        Scaffold(
          //key: _scaffoldKey,
          appBar: AppBar(
            title: Text(
              ("Menu"),
              style: TextStyle(
                  color: Theme.of(context).backgroundColor, fontSize: 15.0),
            ),
            centerTitle: true,
            actions: <Widget>[
              Hero(
                  tag: 'Logo',
                  child: Image.asset('images/UP_LIFE_BLACK.png', scale: 15.1))
            ],
          ),
          drawer: Drawer(
            child: Column(
              children: <Widget>[
                DrawerHeader(
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorDark),
                    child: SingleChildScrollView(
                      physics: NeverScrollableScrollPhysics(),
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.1,
                                  padding: EdgeInsets.only(
                                      bottom:
                                          MediaQuery.of(context).size.height *
                                              0.005),
                                  child: Image.asset('images/chewbacca.png')),
                              Text(
                                ("Chewbacca"),
                                style: TextStyle(
                                    color: Theme.of(context).backgroundColor,
                                    fontSize: 15.0),
                              ),
                              Text("Maromba",
                                  style: TextStyle(
                                      color:
                                          Theme.of(context).primaryColorLight,
                                      fontSize: 16.0))
                            ],
                          )),
                    )),
                ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.local_drink_outlined),
                      title: Text("Calculadora de Água"),
                      onTap: () async {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AguaPage()));
                      },
                    ),
                    ListTile(
                      leading: Icon(CupertinoIcons.barcode_viewfinder),
                      title: Text("Leitor de Código de Barras"),
                      onTap: () async {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LeitorPage()));
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.monitor_weight),
                      title: Text("Calculadora IMC"),
                      onTap: () async {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => ImcPage()));
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.account_circle_rounded),
                      title: Text("Falar com Nutricionista"),
                      onTap: () async {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NutriPage()));
                      },
                    ),
                  ],
                ),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.all(
                            MediaQuery.of(context).size.height * 0.01),
                        child: Align(
                          alignment: FractionalOffset.bottomLeft,
                          child: RaisedButton(
                            color: Theme.of(context).primaryColorDark,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                RotatedBox(
                                  quarterTurns: 2,
                                  child: Icon(Icons.exit_to_app,
                                      color: Colors.white),
                                ),
                                Text(
                                  "   Sair",
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                            onPressed: () async {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Login()));
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Login()),
                                  (Route<dynamic> route) => false);
                            },
                          ),
                        ))),
              ],
            ),
          ),
          body: ListView(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(5),
                  child: Card(
                    color: Colors.grey[100],
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(5),
                        ),
                        Text(
                          'Dica do dia :',
                          style: TextStyle(
                            fontSize: 25,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                        ),
                        Text(
                          result,
                          textAlign: TextAlign.center,
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                        ),
                      ],
                    ),
                  )),
              Container(
                height: MediaQuery.of(context).size.height * 0.4,
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.0000001,
                    top: MediaQuery.of(context).size.height * 0.002),
                child: Image.asset('images/UP_LIFE.png'),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.06,
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.05),
                child: ElevatedButton(
                  child: Text(
                    "Sugestões Alimentares",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => DietaPage()));
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01,
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.05),
                child: ElevatedButton(
                  child: Text(
                    "Treinos",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()));
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01,
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.05),
                child: ElevatedButton(
                  child: Text(
                    "Encontrar Academia",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Maps()));
                  },
                ),
              ),
            ],
          ),
          backgroundColor: Color.fromARGB(255, 220, 220, 220),
        ),
      ],
    );
  }
}
