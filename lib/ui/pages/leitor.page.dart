import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:up_life/ui/pages/func_leitor.page.dart';
import 'package:get/get.dart';

class LeitorPage extends StatefulWidget {
  LeitorPage() {
    Get.put(LeitorController());
  }

  @override
  _LeitorPageState createState() => _LeitorPageState();
}

class _LeitorPageState extends State<LeitorPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Leitor de Código de Barras"),
        ),
        body: SizedBox.expand(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text('Valor do Código de Barras :',
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      fontWeight: FontWeight.bold,
                    )),
            GetBuilder<LeitorController>(
              builder: (controller) {
                return Text(controller.valorCodigoBarras,
                    style: Theme.of(context).textTheme.headline5);
              },
            ),
            Text('\nNome do Produto: ',
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      fontWeight: FontWeight.bold,
                    )),
            GetBuilder<LeitorController>(
              builder: (controller) {
                return Text(controller.nome_produto,
                    style: Theme.of(context).textTheme.headline5);
              },
            ),
            Text('\nO produto possui :',
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      fontWeight: FontWeight.bold,
                    )),
            GetBuilder<LeitorController>(
              builder: (controller) {
                return Text(controller.calorias,
                    style: Theme.of(context).textTheme.headline5!);
              },
            ),
            Text('\nClassificação :',
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      fontWeight: FontWeight.bold,
                    )),
            GetBuilder<LeitorController>(
              builder: (controller) {
                return Text(controller.avaliacao,
                    style: Theme.of(context).textTheme.headline5!);
              },
            ),
            SizedBox(
              height: 30,
            ),
            ElevatedButton.icon(
              icon: Image.asset(
                'images/scanner.png',
                width: 50,
                height: 70,
              ),
              label: Text('Ler Código de Barras',
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(color: Colors.white)),
              onPressed: () {
                Get.find<LeitorController>().escanear_cod();
              },
            )
          ]),
        ));
  }
}
