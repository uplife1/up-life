import 'package:flutter/material.dart';

class JantaPage extends StatefulWidget {
  const JantaPage({Key? key}) : super(key: key);

  @override
  _JantaPageState createState() => _JantaPageState();
}

class _JantaPageState extends State<JantaPage> {
  l_card() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank, size: 60),
            title: Text('Macarrão com frango'),
            subtitle: Text('150g de filé de frango, 70g de macarrão'),
          ),
        ],
      ),
    );
  }

  l_card2() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank, size: 60),
            title: Text('Salmão com Aspargos'),
            subtitle: Text('100g de salmão, 30g de aspargos'),
          ),
        ],
      ),
    );
  }

  l_card3() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank, size: 60),
            title: Text('Salada de frango'),
            subtitle: Text('Salada a vontade, 150g de frango desfiado'),
          ),
        ],
      ),
    );
  }

  l_card4() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.food_bank, size: 60),
            title: Text('Carne Moída'),
            subtitle: Text('100g de carne moída, 50g de arroz, salada'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Jantar"),
        ),
        body: Column(children: <Widget>[
          Text('\nDicas de Jantar\n',
              style: Theme.of(context).textTheme.headline5!),
          l_card(),
          l_card2(),
          l_card3(),
          l_card4(),
        ]));
  }
}
