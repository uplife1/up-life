import 'package:flutter/material.dart';

class AguaPage extends StatefulWidget {
  const AguaPage({Key? key}) : super(key: key);

  @override
  _AguaPageState createState() => _AguaPageState();
}

class _AguaPageState extends State<AguaPage> {
  var result = "";
  var peso = "";

  bool val = false;
  onSwitchValueChanged(bool newVal) {
    setState(() {
      val = newVal;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Calculadora de Água")),
      body: ListView(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.25,
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.02,
                top: MediaQuery.of(context).size.height * 0.05),
            child: Image.asset('images/water.png'),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: TextFormField(
              decoration: InputDecoration(
                labelText: "Informe o seu peso ",
              ),
              onChanged: (text) {
                peso = text;
              },
              keyboardType: TextInputType.number,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              result,
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: ElevatedButton(
              child: Text(
                "Calcular",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                setState(() {
                  double agua = (double.parse(peso) * 35) / 1000;
                  result =
                      "É recomendável que você beba ${agua} L de água por dia";
                });
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: SwitchListTile(
                title: const Text('Ativar lembrete para beber água'),
                value: val,
                onChanged: (newVal) {
                  onSwitchValueChanged(newVal);
                  print(val);
                }),
          ),
        ],
      ),
    );
  }
}
