import 'package:flutter/material.dart';
import 'package:up_life/ui/pages/menu.page.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String email = '';
  String password = '';
  var result = '';

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueGrey[50],
      child: Center(
          child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Hero(
              tag: 'Logo',
              child: Image.asset("images/UP_LIFE.png", scale: 4),
            ),
            TextField(
              onChanged: (text) {
                email = text;
              },
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  labelText: 'Email', border: OutlineInputBorder()),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
            ),
            TextField(
              onChanged: (text) {
                password = text;
              },
              obscureText: true,
              decoration: InputDecoration(
                  labelText: 'Senha', border: OutlineInputBorder()),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
            ),
            Padding(
              padding: const EdgeInsets.all(7),
              child: Text(
                result,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.red[700]),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                if (email == 'guigirotto99@hotmail.com' && password == '123') {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MenuPage()));
                } else {
                  setState(() {
                    result =
                        'Senha ou email incorreto, informe os dados novamente !';
                    print(result);
                  });
                }
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 40.0),
                child: Text('Entrar'),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 40.0),
            ),
            ElevatedButton(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 94.0),
                child: Text('Cadastrar'),
              ),
              onPressed: () {},
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.blue.shade900),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 60.0),
                child: Text('Entrar com Facebook'),
              ),
              onPressed: () {},
            ),
          ],
        ),
      )),
    );
  }
}
