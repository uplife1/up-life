import 'package:flutter/material.dart';

class CafeDaManhaPage extends StatefulWidget {
  const CafeDaManhaPage({Key? key}) : super(key: key);

  @override
  _CafeDaManhaPageState createState() => _CafeDaManhaPageState();
}

class _CafeDaManhaPageState extends State<CafeDaManhaPage> {
  l_card() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.free_breakfast, size: 60),
            title: Text('Tapioca'),
            subtitle: Text('30g de tapioca, 20g queijo, 20g peito de peru.'),
          ),
        ],
      ),
    );
  }

  l_card2() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.free_breakfast, size: 60),
            title: Text('Ovo Mexido'),
            subtitle: Text('2 ovos, 20ml de leite'),
          ),
        ],
      ),
    );
  }

  l_card3() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.free_breakfast, size: 60),
            title: Text('Frutas'),
            subtitle: Text('100g de frutas de sua escolha'),
          ),
        ],
      ),
    );
  }

  l_card4() {
    return Card(
      color: Colors.grey[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.free_breakfast, size: 60),
            title: Text('Crepioca'),
            subtitle: Text('1 ovo, 30g tapioca'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Café da manha"),
        ),
        body: Column(children: <Widget>[
          Text('\nDicas de Café da Manhã\n',
              style: Theme.of(context).textTheme.headline5!),
          l_card(),
          l_card2(),
          l_card3(),
          l_card4(),
        ]));
  }
}
